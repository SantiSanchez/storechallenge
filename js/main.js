$('#loginButton').on('click', function (e) {
    const modal = document.getElementById('loginModal');
    const span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";

    span.onclick = function () {
        modal.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});
/*
    Funcion encargada de abrir el modal de registro de usuario
*/
$('#registerButton').on('click', function (e) {
    const modal = document.getElementById('registerModal');        
    const span = document.getElementsByClassName("close")[1];
    modal.style.display = "block";

    span.onclick = function () {
        modal.style.display = "none";
    }
    
    window.onclick = function (event) {        
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});

/*
    Funcion para abrir el modal del carrito de compras
*/
$('#cartButton').on('click', function (e) {
    const modal = document.getElementById('cartModal');
    const span = document.getElementsByClassName("close")[2];
    modal.style.display = "block";

    span.onclick = function () {
        modal.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});
/*
    Metodo encargado de guardar el usuario,nombre,contraseña.
*/
$('#register').on('click', function (e) {
    const modal = document.getElementById('registerModal');
    var username = $('#usernameTextRegister').val();
    var name = $('#nameText').val();
    var password = $('#passwordTextRegister').val();
    var repeatPassword = $('#repeatPasswordText').val();
    if (password != repeatPassword) {
        $('#passwordTextRegister').addClass("password-error");
        $("input#passwordTextRegister").after('<p class="password-error-text">Las contraseñas no coinciden</p>');
        return;
    }
    localStorage.setItem("nameText", name);
    localStorage.setItem("usernameText", username);
    localStorage.setItem("passwordText", password);
    modal.style.display = "none";
});
/*
    Funcion encargada de verificar el usuario ingresado y la contraseña con el usuario guardado
*/
$('#login').on('click', function (e) {
    const modal = document.getElementById('loginModal');
    var usernameEntered = $('#usernameTextLogin').val();
    var passwordEntered = $('#passwordText').val();
    var usernameSaved = localStorage.getItem("usernameText");
    var passwordSaved = localStorage.getItem("passwordText");
    var nameSaved = localStorage.getItem("nameText");

    if (usernameEntered == usernameSaved && passwordEntered == passwordSaved) {        
        modal.style.display = "none";
        $('#userName').append(usernameSaved);   
    } else {
        $("input#usernameTextLogin").before('<p class="password-error-text">Usuario o contraseña no validos</p>');
    }
});
/*
    Event listener para agregar productos al carrito de compras
*/
$(document).on("click", "#addToCart", function () {                
    var product = $(this).parent().text();
    var productImage = $(this).parent().find('img').attr('src');
    $('#productDetail').append('<p >' + product + '</p><img src="' + productImage + '">');                
});
/*
    Metodo para hacer la peticion de la API
*/
$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: 'https://jsonplaceholder.typicode.com/photos',
        success: function success(response) {
            printProducts(response);
            pagination(response.length-4901,9);            
        }
    });
});

/*
    Eventlistener encargado de la busqueda de productos
*/

$('#search').on("click",function (e) {    
    searchString = $('#searchInput').val().toLowerCase();
    searchArray = searchString.split(' ');
    var flag = 0;
    var len = searchArray.length;
    var $itemList = $('#products').find('.product');        
    $itemList.each(function (index, elem) {
        $eleli = $(elem)
        $eleli.removeClass('hideThisLine');
        $eleli.removeClass('showThisLine');
        var oneLine = $eleli.text().toLowerCase();
        match = true;
        sal = len;
        while (sal--) {
            if (oneLine.indexOf(searchArray[sal]) == -1) {
                match = false;
            }
        }
        if (!match) {
            $eleli.addClass('hideThisLine');
        }
        if (match) {
            $eleli.addClass('showThisLine');
        }
        if(searchString == ""){            
            $eleli.addClass('showThisLine');
            flag=1;
        }
    });    
    if(flag == 0){
        pagination(99,99); 
    }
    else{
        pagination(99,9);
    }
    $('.dontShow').removeClass('dont-show');
    $('.showThisLine').removeClass('dont-show');
    $('.hideThisLine').addClass('dont-show');
});
/*
    Metodo que se encarga de poner en pantalla el numero de paginas
*/
function pagination(length,itemsPerPage) {    
    var numbers = ($("#products div").length - (length/3)) / itemsPerPage;    
    numbers = parseInt(numbers.toFixed(0));
    var elements = "";
    $('#pagination_history').empty();
    for (var i = 1; i < numbers + 1; i++) {
        elements += "<li><a onclick='pagination_history(" + i + "," + itemsPerPage + ");'>" + i + "</a></li>";
    }
    $('#pagination_history').append(elements);
    pagination_history(1, itemsPerPage);
}
/*
    Metodo encargado de mostrar los productos por paginas
*/
function pagination_history(number, itemsPerPage) {
    $(".product").css('display', 'none');
    var number_show = number * itemsPerPage;
    for (var i = 0; i < itemsPerPage; i++) {
        number_show--;
        $(".product:eq(" + number_show + ")").css('display', 'block');
    }
}
/*
    Metodo que se encarga de imprimir los productos
*/
function printProducts(response) {
    var row = '<div class="row">';
    var hideClass = "";
    var searchString;
    var count = 0;
    for (let i = 0; i < response.length - 4901; i++) {

        row += '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 product">' + response[i].title + '<p></p>';
        row += '<p> Precio:' + (response[i].id + response[i].id) * 100 + '</p>';
        row += '<img class="img-responsive" src="' + response[i].thumbnailUrl + '"><p></p>';
        row += '<button id="addToCart" class="btn-add-to-cart"><i class="fa fa-cart-plus fa-2x"></i></button></div>';
        count++;
        if (count == 3) {
            row += '</div>';
            $('div#products').append(row);
            row = '<div class="row">';
            count = 0;
        }
    }

}
